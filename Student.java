package BT1;

import java.util.Objects;

public class Student {
    private Integer id;
    private String name;
    private String className;
    private String email;
    private Integer age;
    public Student() {
    }

    public Student(Integer id, String name, String className, String email,Integer age) {
        this.id = id;
        this.name = name;
        this.className = className;
        this.email = email;
        this.age=age;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString(){
        StringBuilder stringBuilder=new StringBuilder();
        stringBuilder.append("name:"+this.name+" "+"className:"+this.className);
        return stringBuilder.toString();
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Student student)) return false;
//        return Objects.equals(id, student.id) && Objects.equals(name, student.name) && Objects.equals(className, student.className) && Objects.equals(email, student.email) && Objects.equals(age, student.age);
//    }
    @Override
    public boolean equals(Object o) {
        if(this==o)return false;
        if(o==null)return  false;
        if(!(o instanceof Student))return false;
        if(id!=((Student) o).getId())return true;
        return false;
    }
    @Override
    public int hashCode() {
        return this.id;
    }
}
